﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Teapot.Models;
using Teapot.Services;

namespace Teapot.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TeapotController : ControllerBase
    {
        private IBrewingService _service;

        public TeapotController(IBrewingService service)
        {
            this._service = service;
        }
        
        [HttpGet("menu")]
        public ActionResult<IEnumerable<string>> Menu()
        {
            return Ok(_service.GetMenu());
        }
        
        [HttpGet]
        public ActionResult<string> Get()
        {
            return StatusCode(418, "This response is short and stout. This is my handle, POST is my spout. GET me over and pour me out!");
        }
        
        [HttpGet("customer/{name}")]
        public ActionResult<IEnumerable<DrinkRecord>> Get([FromRoute] string name)
        {
            var serving = _service.ServeDrinks(name);
            return Ok(serving);
        }

        [HttpPost]
        public ActionResult<Guid> Brew([FromBody] DrinkRequestModel model)
        {
            var parsed = Enum.TryParse(model.Type, out DrinkType drink);
            if (!parsed)
            {
                return BadRequest("Invalid Drink Type!");
            }
            var id = _service.BrewDrink(model.Name, drink);
            return Ok(id);
        }
        
        [HttpPut("order/{drinkId}")]
        public ActionResult<IEnumerable<DrinkRecord>> Replace([FromRoute] Guid drinkId, [FromBody] DrinkRequestModel model)
        {
            var parsed = Enum.TryParse(model.Type, out DrinkType drink);
            if (!parsed)
            {
                return BadRequest("Invalid Drink Type!");
            }
            if(!_service.ChangeDrink(drinkId, model.Name, drink)) return NotFound("No unserved drink with that ID");
            return Ok();
        }
        
        [HttpDelete("customer/{name}/order/{drinkId}")]
        public ActionResult<IEnumerable<DrinkRecord>> Cancel([FromRoute] Guid drinkId, [FromRoute] string name)
        {
            if(!_service.CancelDrink(drinkId, name)) return NotFound("No unserved drink with that ID");
            return NoContent();
        }
    }
}