using System;

namespace Teapot.Models
{
    public class DrinkRecord
    {
        public DateTime Date { get; set; }

        public string Customer { get; set; }
        
        public DrinkType Drink { get; set; }
        
        public Guid Id { get; set; }

        public bool Served { get; set; } = false;

    }
}