namespace Teapot.Models
{
    public class DrinkResponse
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public double TempCelcius { get; set; }
    }
}