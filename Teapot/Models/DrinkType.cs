namespace Teapot.Models
{
    public enum DrinkType
    {
        Mocha, Cappuccino, Americano, Latte, EarlGrey, Darjeeling, GreenTea, Oolong
    }
}