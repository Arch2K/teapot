using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using Teapot.Models;

namespace Teapot.Repositories
{
    public class DrinkRepository: IDrinkRepository
    {
        private Dictionary<string, List<DrinkRecord>> _drinkOrders = new Dictionary<string, List<DrinkRecord>>();
        
        public DrinkRepository(){}
        public IEnumerable<DrinkRecord> ServeDrinks(string name)
        {
            var drinks = _drinkOrders[name].Where(x => !x.Served);
            foreach (var drinkRecord in drinks)
            {
                drinkRecord.Served = true;
            }
            return drinks;
        }

        public IEnumerable<DrinkRecord> GetDrinkHistory(string name)
        {
            throw new NotImplementedException();
        }

        public void MakeDrink(DrinkRecord drink)
        {
            if(!_drinkOrders.ContainsKey(drink.Customer)) _drinkOrders.Add(drink.Customer, new List<DrinkRecord>());
            _drinkOrders[drink.Customer].Add(drink);
        }

        public bool CancelDrink(Guid id, string customer, out DrinkRecord drink)
        {
            drink = null;
            if (!_drinkOrders.ContainsKey(customer)) return false;
            for(var i = 0; i < _drinkOrders[customer].Count; i++)
            {
                if (!_drinkOrders[customer][i].Served && _drinkOrders[customer][i].Id == id)
                {
                    drink = _drinkOrders[customer][i];
                    _drinkOrders[customer].RemoveAt(i);
                    return true;
                }
            }

            return false;
        }
    }
}