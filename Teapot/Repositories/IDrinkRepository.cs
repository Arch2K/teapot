using System;
using System.Collections.Generic;
using Teapot.Models;

namespace Teapot.Repositories
{
    public interface IDrinkRepository
    {
        IEnumerable<DrinkRecord> ServeDrinks(string name);
        IEnumerable<DrinkRecord> GetDrinkHistory(string name);
        void MakeDrink(DrinkRecord drink);

        bool CancelDrink(Guid id, string customer, out DrinkRecord drink);
    }
}