using System;
using System.Collections.Generic;
using System.Linq;
using Teapot.Models;
using Teapot.Repositories;

namespace Teapot.Services
{
    public class BrewingService: IBrewingService
    {
        private IDrinkRepository _repo;

        public BrewingService(IDrinkRepository repo)
        {
            this._repo = repo;
        }

        public IEnumerable<string> GetMenu()
        {
            return Enum.GetNames(typeof(DrinkType));
        }

        public Guid BrewDrink(string name, DrinkType type)
        {
            var time = DateTime.Now;
            var id = Guid.NewGuid();
            _repo.MakeDrink(new DrinkRecord{Id = id, Customer = name, Drink = type, Date = time});
            return id;
        }

        public bool CancelDrink(Guid id, string customer)
        {
            return _repo.CancelDrink(id, customer, out _);
        }

        public bool ChangeDrink(Guid id, string customer, DrinkType type)
        {
            // Get Current Time
            var time = DateTime.Now;
            // Try to cancel the existing drink
            var cancelled = _repo.CancelDrink(id, customer, out DrinkRecord drink);
            
            if (!cancelled) return false;
            // Create a new drink to replace the old one
            _repo.MakeDrink(new DrinkRecord{Id = id, Customer = drink.Customer, Drink = type, Date = time});
            return true;
        }

        public IEnumerable<DrinkResponse> ServeDrinks(string name)
        {
            var drinks = _repo.ServeDrinks(name);
            var serving = drinks.Select(x => 
                new DrinkResponse 
                {
                    Name = x.Customer, 
                    Type = x.Drink.ToString(), 
                    TempCelcius = CalculateTemp(DateTime.Now - x.Date)
                }
            );
            return serving;
        }

        private double CalculateTemp(TimeSpan time)
        {
            var temp = 28 + 52 * Math.Pow(Math.E, -0.0462098 * time.TotalMinutes);
            return temp;
        }
    }
}