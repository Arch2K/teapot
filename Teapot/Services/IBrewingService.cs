using System;
using System.Collections.Generic;
using Teapot.Models;

namespace Teapot.Services
{
    public interface IBrewingService
    {
        IEnumerable<string> GetMenu();
        Guid BrewDrink(string name, DrinkType type);

        bool CancelDrink(Guid id, string customer);

        bool ChangeDrink(Guid id, string customer, DrinkType type);

        IEnumerable<DrinkResponse> ServeDrinks(string name);
    }
}